# Ali Göktas

Meine Webseite:
[ali-portfolio.ch](https://ali-portfolio.ch)

**Aktuell:**
Zur Zeit arbeite ich intensiv mit Laravel.

**Mein Stack:**
Laravel, PHP, MySQL, MariaDB, OOP  
**Ebenfalls:**
HTML, CSS, TailwindCSS, (modernes) JS, Git/GitLab, JSON, API, VSCode (inkl. Tastaturshortcuts zum produktiver Arbeiten), ...  
**Etwas Erfahrung in:**
Assembler, CodeIgniter, Docker, Doctrine, Java, Python, Typo3, Zend 2, ...

Angefangen 1996 mit dem Programmieren, hobbymässig, in meiner Freizeit, habe ich letztlich mein Hobby zum Beruf gemacht und eine Ausbildung zum Fachinformatiker absolviert. 2013.  

#### abgeschlossene grössere Projekte sind:
- [Fullstack Bootcamp](https://udemy.com/course/the-ultimate-fullstack-web-development-bootcamp)
- Schachprogramm
- [LT-Webseite](https://gitlab.com/ali-goektas/megaprojekt-lt)

#### abgeschlossene kleinere Projekte sind:
- [Bau E1 GmbH](https://bau-e1.ch)
- "Texte glz. nach mehreren Begriffen durchsuchen"-Funktion
- Modding div. Spiele (Star Trek Armada, ...)
- u. v. m.